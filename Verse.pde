class Verse {
  Book book;
  Integer chapter = -1;
  Integer verse = -1;
  String content = "";
  int wordCount = -1;
  
  String matches[] = null;

  Verse() {
  }

  Verse(Book b, Integer c, Integer v, String val) {
    book = b;
    chapter = c;
    verse = v;
    content = val;
    //println("Verse instantiated: " + str(b) + " " + str(c) + ":" + str(v) + " = " + val);
  }

  int getWordCount() {
    if (wordCount > 0) {
      return(wordCount);
    } else {
      return(content.split(" ").length);
    }
  }

  String toRef() { 
    return(book.name + " " + str(chapter) + ":" + str(verse));
  }

  boolean contains(String str) {
    try {    
      matches = match(this.content, str);
      if (matches != null) {
        return(matches.length > 0);
      } else {
        return(false);
      }
    } catch (Exception ex) {
      return(false);
    }
  }
    
  public String toString() {
    return("Book: " + book.name + "; Chp: " + str(chapter) + "; Verse: " + str(verse));
  }
}