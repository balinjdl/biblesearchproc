Testament t;
float buffer = width/50.0; // Pixels
// boolean useFixedBookWidth = true;
boolean useFixedBookWidth = false;
float chapterWidth = 0.0;
boolean isBookActive = false;
Book activeBook;
Chapter activeChapter = null;

View view;

Search search;
int count = 135;

PFont serif;
int currentFontIndex;
int MIN_FONT_SIZE = 8;
int DEFAULT_FONT_SIZE_INDEX = 3;
PFont[] fonts;
int FONTS_AVAILABLE = 5;
int currentFontSize = 12;

int MIN_SEARCH_STR_LENGTH = 2;

void setup() {
  size(1000, 800);  
  frameRate(20);
  //fullScreen();
  surface.setResizable(true);

  t = new Testament("bible.txt");
  
  fonts = new PFont[FONTS_AVAILABLE + 1];
  for (int i = 0; i < FONTS_AVAILABLE; i++) {
    fonts[i] = createFont("Serif", MIN_FONT_SIZE + (i * 2), true);
  }
  currentFontIndex = 3;
  textFont(fonts[currentFontIndex]);

  search = new Search();
  view = new View();
};

void keyPressed() {  
  //println("key = " + key + " // keyCode " + keyCode + " // key == CONTROL? " + (key == CONTROL) + " // key == CODED? " + (key == CODED));

  if (key == CODED) {
    view.inControl = false;
    if (keyCode == UP) {
      changeFontSize(1);
    } else if (keyCode == DOWN) {
      changeFontSize(-1);
    } else if (keyCode == RIGHT) {
      //println("increasing word width");
      view.increaseWordWidth();
    } else if (keyCode == LEFT) {
      //println("decreasing word width");
      view.decreaseWordWidth();
    } else if (keyCode == 157) {
      // keyCode 157 = COMMAND key
      view.ignoreKeys(true);
    } else if (keyCode == CONTROL) {
      view.inControl = true;
      view.ignoreKeys(true);
      //println("CONTROL key pressed; inControl = " + inControl);
    } else {
      println("unknown key pressed: " + str(keyCode));
    }
  } else if (key == BACKSPACE) {
    if (!view.ignoreKeys) {
      search.shorten();
      executeSearch();
    }
  } else {
    //println("inControl = " + inControl);
    if (view.inControl) {
      if (key == 'f' || keyCode == 70) {
        view.freezeDisplay = !view.freezeDisplay;
        //println("freezeDisplay = " + freezeDisplay);
      } else if (key == 'a' || keyCode == 65) {
        view.showHitsOnly = !view.showHitsOnly;
      //} else if (key == BACKSPACE) {
      //  search.restart();
      } else {
        println("Unrecognized inControl combination: " + key + " - code: " + keyCode);
      }
    } else if (!view.ignoreKeys) {
      search.append(key);
      if (str(key).equals("(")) {
        search.inParens = true;
        // But don't execute the search
      } else if (str(key).equals(")")) {
        search.inParens = false;
      }
      if (!search.inParens)
        executeSearch();
    }
  }
}

void keyReleased() {
  //println("released key " + (key));
  if (keyCode == 157) {
    //println("turning off ignoreKeys (CMD released)");
    view.ignoreKeys(false);
  } else if (keyCode == CONTROL) {
    //println("turning off ignoreKeys & inControl (CONTROL released)");
    view.inControl = false;
    view.ignoreKeys(false);
  }
}

void mousePressed() {
  view.freezeDisplay = !view.freezeDisplay;
}

void executeSearch() {
  if (search.str.length() > MIN_SEARCH_STR_LENGTH && !search.inParens) {
    search.restart();
    for (Book b : t.books) {
      for (Chapter c : b.chapters) {
        for (Verse v : c.verses) {
          //if (v.content.indexOf(search.str) > 0) {
          //println("Searching for " + search.str);
          if (v.contains(search.str)) {
            search.stats.add(new SearchHit(search, v));
            //println("Adding hit for " + search.str + " @ " + str(v.book.number) + ":" + str(v.chapter));
          }
        }
      }
    }
  }
  search.stop();
  //println("executeSearch (for '" + search.str + "') finished: Found " + str(search.stats.count()) + " hits: " + search.stats.toString());
}

void changeFontSize(int diff) {
  int newVal = currentFontIndex + diff;
  if (newVal < FONTS_AVAILABLE && newVal >= 0) {
    currentFontIndex = newVal;
    textFont(fonts[currentFontIndex]);
    // println("Changed text font index to " + str(newVal));
  }
  currentFontSize = MIN_FONT_SIZE + (currentFontIndex * 2);
}

void draw() {
  view.reset();

  stroke(1);
  background(255);
  for (Book b : t.books) {
    b.display(search, view);
  }

  if (isBookActive) {
    displayChapter(activeChapter);
  }

  displayConfig();
}

void displayConfig() {
  textSize(currentFontSize + 1);
  stroke(0);
  fill(250, 250, 200);

  float x = t.books.get(13).getBoundary().w + (view.buff * 2);
  float y = t.books.get(8).getBoundary().y;
  float h = t.books.get(17).getBoundary().y - y;
  //println("config square at " + str(x) + " x " + str(y) + "!");
  rect(x, y, width - (x + view.buff), h);

  stroke(1);
  if (view.freezeDisplay) { 
    fill(255, 0, 0);
  } else {
    fill(0);
  }
  text(view.freezeDisplay ? "Display frozen" : "Active display", width - 250, y + 15);
  if (view.showHitsOnly) { 
    fill(255, 0, 0);
  } else {
    fill(0);
  }
  text(view.showHitsOnly ? "Showing hits only" : "Showing all content", width - 150, y + 15);
  
  textSize(currentFontSize + 4);
  
  fill(0);
  text("Searching for '" + search.toString() + "'", x + 5, y + 15);
  textSize(currentFontSize - 2);
  text(str(search.stats.hits.size()) + " matches [" + search.stats.toString() + "]", x + 5, y + 20, width - x + 5 - (2 * view.buff), 90 - 20);
}

void displayChapter(Chapter c) {
  if (!view.showHitsOnly || search.hitInChapter(c)) {
    float x = t.books.get(41).getBoundary().w + (view.buff * 2);
    float y = t.books.get(28).getBoundary().y;
    stroke(1);
    fill(255);
    rect(x, y, width - (x + view.buff), height - (y + view.buff));

    textSize(currentFontSize + 4);
    stroke(1);
    fill(0);
    text(c.book.name + " " + str(c.number), x + view.buff/2, y + 15);
    textSize(currentFontSize);
    text(c.toString(search, view.showHitsOnly), x + view.buff/2, y + 25, (width - (x + 5 + (view.buff * 2))), height - (y + (view.buff * 2) + 5));
  }
}

void mouseMoved() {
  if (!view.freezeDisplay) {
    isBookActive = false;

    for (Book b : t.books) {
      if (b.mouseInside(mouseX, mouseY)) {
        activeChapter = b.chapterInside(mouseX, mouseY);
        //println("Inside " + t.books[b.number - 1].name + 
        //  " Chp #" + str(activeChapter) + " " + 
        //  t.books[b.number-1].boundary.toString());
        isBookActive = true;
        return;
      }
    }
  }
}