class SearchStats {
  BibleUtils utils = new BibleUtils();
  
  ArrayList<SearchHit> hits = new ArrayList<SearchHit>(10);
  ArrayList<Verse> hits_v = new ArrayList<Verse>(10);

  boolean collectStats = false;
  boolean freezeStats = false;

  SearchStats() {
  }

  SearchStats reset() {
    hits.clear();
    hits_v.clear();
    return(this);
  }

  SearchStats freeze() { 
    return(stop());
  }

  SearchStats stop() {
    freezeStats = true;
    return(this);
  }

  SearchStats start() {
    freezeStats = false;
    return(reset());
  }

  SearchStats add(SearchHit hit) {
    hits.add(hit);
    hits_v.add(hit.v);
    //println(" add(...) --> updating hits_flat to " + getHitsFlat());
    //println("adding(hit): " + hit.toString());
    //println("... append(" + str(hit.v.book.number) + ":" + str(hit.v.chapter) + ")");
    return(this);
  }

  int hitCount() { 
    return(count());
  }

  int count() { 
    return(hits.size());
  }

  String getHitsFlat() {
    StringList results = new StringList();
    for (Verse v: hits_v) {
      results.append(v.toRef());
    }
    return(join(results.array(), ", "));
  }

  String getVerseHits() {
    ArrayList<String> hitVerses = new ArrayList<String>();
    for (SearchHit h : hits) {
      hitVerses.add(h.toString());
    }
    return(join(hitVerses.toArray(new String[0]), ","));
  }

  String toString() {
    return(getHitsFlat());
  }
}