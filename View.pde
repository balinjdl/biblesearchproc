class View {
  float wordW = 0.02; // TODO: Calc based on window width
  float buff = 20;
  float bookH;
  float prevHeight = -1;

  boolean changed = false;

  color COLOR_HIT_IN_CHAPTER = color(255, 0, 0);
  color COLOR_BORDER_DEFAULT = color(0, 0, 0);
  color COLOR_FILL_DEFAULT = color(200, 200, 200);
  color COLOR_TRANSPARENT = color(255, 255, 255);
  color COLOR_BORDER_LIGHT = color(200, 200, 200);

  int lastW = -1;
  int lastH = -1;

  boolean freezeDisplay = false;
  boolean ignoreKeys = false;
  boolean inControl = false;
  boolean showHitsOnly = false;

  View() {
    reset();
  }

  View setBuffer(float newB) {
    if (buff != newB) {
      buff = newB;
      changed = true;
    }
    return(this);
  }

  float getBuffer() { 
    return(buff);
  }

  boolean ignoreKeys(boolean val) {
    ignoreKeys = val;
    return(ignoreKeys);
  }

  float _calcBookHeight() {
    if (height != prevHeight) {
      //println("new height calculated, based on changing height (was " + str(prevHeight) + " is now " + str(height) + ")");
      prevHeight = height;
      return((height - (buff * 2)) / 66);
    } else {
      return(bookH); // no need to recalc
    }
  }

  View reset() {
    return(resetBookHeight().clean());
  }

  View clean() {
    changed = false;
    return(this);
  }

  View resetBookHeight() {
    return(setBookHeight(_calcBookHeight()));
  }

  View setBookHeight(float newH) {
    bookH = newH;
    //println("bookH = " + str(bookH) + "!");
    return(this);
  }

  View resetWordWidth() {
    return(setWordWidth(_calcWordWidth()));
  }

  View setWordWidth(float newW) {
    wordW = newW;
    return(this);
  }

  float _calcWordWidth() {
    return(wordW);
  }

  View increaseWordWidth() { 
    wordW = wordW * 1.1; 
    return(this);
  }
  View decreaseWordWidth() { 
    wordW = wordW * 0.9;
    return(this);
  }

  float getWordWidth() { 
    return(wordW);
  }

  color getBookColor(boolean hit) {
    if (hit) {
      return(COLOR_HIT_IN_CHAPTER);
    } else {
      return(COLOR_FILL_DEFAULT);
    }
  }

  boolean isClean() {
    if (height != prevHeight) {
      changed = true;
    }
    return(changed);
  }
}