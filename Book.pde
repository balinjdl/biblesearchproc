class Book {
  int number = -1;
  String name;
  Chapter[] chapters;
  Boundary boundary;
  boolean boundarySet = false;
  Testament testament;

  Book (String n, int num) {
    name = n;
    number = num;
    // println("Book instantiated: " + name + " (#" + str(number) + ")");
    chapters = new Chapter[0];
  };

  Book setTestament(Testament t) {
    testament = t;
    return(this);
  }

  void display(Search search, View view) {
    float w = -1;

    float x = view.buff;
    float y = view.buff + ((number - 1) * view.bookH);

    for (Chapter c : chapters) {
      w = c.getWordCount() * view.wordW;
      fill(view.getBookColor(search.hitInChapter(c)));
      if (!c.hasBoundary()) {
        c.setBoundary(new Boundary(x, y, w, view.bookH));
        //_expandBoundary(x, y, w, view.bookH);
      } else if (!view.isClean()) {
        //println("Re-Setting boundary for Book #" + 
        //  str(number) + " & chapter #" + 
        //  str(c.number) + " = x:" + 
        //  str(x) + ", y:" + 
        //  str(y) + ", w:" + 
        //  str(w) + ", h:" + 
        //  str(view.bookH));
        c.setBoundary(new Boundary(x, y, w, view.bookH));
      }
      c.display(search);
      // Move out to start the next chp at the end of the last chp
      x = x + w;
    }
    if (!view.isClean()) {
      _setBoundary();
    }
  }

  void addVerse(Verse v) {
    if (chapters.length < v.chapter) {
      addChapter();
    }
    chapters[chapters.length - 1].addVerse(v).setNumber(v.chapter);
  }

  void addChapter() {
    chapters = (Chapter[]) expand(chapters, chapters.length + 1);
    chapters[chapters.length - 1] = new Chapter();
    chapters[chapters.length - 1].setBook(this);
  }

  boolean mouseInside(float x, float y) {
    return(getBoundary().inside(x, y));
  }

  Chapter chapterInside(float x, float y) {
    if (mouseInside(x, y)) {
      for (Chapter c : chapters) {
        if (c.mouseInside(x, y)) {
          return(c);
        }
      }
    }
    println("ERROR in Book.chapterInside - can't find chapter in " + str(x) + " x " + str(y));
    return(null);
  }

  Boundary getBoundary() {
    if (!boundarySet()) {
      _setBoundary();
    }
    return(boundary);
  }

  boolean boundarySet() { 
    return(boundarySet);
  }

  void _setBoundary() {
    // Calc boundary from chapter boundaries
    float minX, maxX, minY, maxY;
    minX = minY = Float.MAX_VALUE;
    maxX = maxY = Float.MIN_VALUE;

    minX = maxX = chapters[0].boundary.x;
    minY = maxY = chapters[0].boundary.y;

    for (Chapter c : chapters) {
      if (c.hasBoundary()) {
        // Expand X and Y
        maxX = maxX + c.boundary.w;
        maxY = c.boundary.y + c.boundary.h;
      }
    }

    //println("Setting boundary for book #" + str(number) + " = x:" + str(minX) + " - y:" + str(minY) + " - w:" + str(maxX - minX) + " - h:" + str(maxY - minY));
    _setBoundary(new Boundary(minX, minY, (maxX - minX), (maxY - minY)));
  }

  void _setBoundary(Boundary b) {
    boundary = b;
    boundarySet = true;
    // println("Setting boundary for " + name + " to " + str(boundary.x) + " x " + str(boundary.y));
  }

  void update() {
    rect(boundary.x, boundary.y, boundary.w, boundary.h, 2);
  }

  int verseCount() {
    int total = 0;
    for (Chapter c : chapters) {
      total = total + c.verses.length;
    }
    return(total);
  }
}