# Bible Search and display application (in Processing)
Search for text in the Bible (OT, NT, or both). Displays matches via highlight.

## Installation
1. Install Processing3 (https://processing.org/)
1. Download all files in repository
1. Secure a text file of the Bible books you want to use - OT, NT, or full - in ./data/

## Usage
Run BibleSearchProc.pde in Processing3

## History
6/7/2016: Created repository with initial version

## License
See License.txt
