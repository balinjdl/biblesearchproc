class Testament {
  String filename;
  String[] raw_contents;

  BibleUtils utils = new BibleUtils();

  int BOOK_COUNT = utils.book_names.length;
  ArrayList<Book> books = new ArrayList<Book>();
  Chapter[] current_book;
  int chapterCount = 0;
  int book_num = -1;

  // Formats:
  // # = number
  // A = letter(s)
  // ... = verse content
  int LINE_FORMAT_A = 1; // # # # ...
  int LINE_FORMAT_B = 2; // AAA #:# ...

  //int lineFormat = LINE_FORMAT_A;
  int lineFormat = LINE_FORMAT_B;

  String _prevBook = "nothing";
  int _bookNum = 0;

  Book currBook = null;
  //Search search;

  Testament(String fn) {
    filename = fn;
    for (String line : loadStrings(filename)) {
      parse_verse(line);
    }
  }

  Book getCurrBook() { 
    return(currBook);
  }

  void parse_verse(String raw) {
    String content = "";
    Verse v = null;
    String[] vals;
    if (lineFormat == LINE_FORMAT_A) {
      vals = split(raw, ",");
      for (int i = 3; i < vals.length; ++i) {
        content = content + vals[i];
      }
      //print(join(vals, ", "));
      if (!vals[0].equals(_prevBook)) { // New book
        currBook = new Book(utils.getBookNames()[int(vals[0])-1], int(vals[0])-1).setTestament(this);
        books.add(currBook);
        _prevBook = vals[0];
      }
      currBook.addVerse(new Verse(getCurrBook(), int(vals[1]), int(vals[2]), content));
    } else if (lineFormat == LINE_FORMAT_B) {
      vals = split(raw, " ");
      for (int i = 2; i < vals.length; ++i) {
        content = content + vals[i] + " ";
      }
      if (!vals[0].equals(_prevBook)) { // New book
        currBook = new Book(utils.getBookNames()[_bookNum], _bookNum).setTestament(this);
        books.add(currBook);
        _bookNum = _bookNum + 1;
        _prevBook = vals[0];
      }
      String[] cv = split(vals[1], ":");
      currBook.addVerse(new Verse(getCurrBook(), int(cv[0]), int(cv[1]), content));
    } else {
      println("ERROR: Unrecognized lineFormat value");
    }
  }

  int verseCount() {
    return(999);
    //return(contents.length);
  }

  int chapterCount() {
    if (chapterCount == 0) {
      int chp = 0;
      for (Book b : books) {
        // print("chapterCount: Increasing by " + str(b.chapters.length));
        chp = chp + b.chapters.length;
        // println(" to " + str(chp));
      }			
      chapterCount = chp;
    }
    // println("t.chapterCount returning " + str(chp));
    return(chapterCount);
  }
}