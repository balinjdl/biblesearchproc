class Boundary {
  float x, y, w, h;
  boolean set = false;
  boolean setX, setY, setW, setH;

  Boundary() {
    x = y = w = h = -1;
    _set(false);
  }

  Boundary(float inX, float inY, float inW, float inH) {
    x = inX;
    y = inY;
    w = inW;
    h = inH;

    _set(true);
  }

  String toString() {
    return("X:" + str(x) + "; Y:" + str(y) + "; W:" + str(w) + "; H:" + str(h));
  }

  boolean _set(boolean val) { 
    set = setX = setY = setW = setH = val; 
    return(val);
  }

  boolean set(float inX, float inY, float inW, float inH) {
    x = inX;
    y = inY;
    w = inW;
    h = inH;

    return(_set(true));
  }

  boolean setX(float inX) { 
    x = inX;
    setX = true;
    if (setX & setY & setW & setH) { 
      _set(true);
    }
    return(set);
  }

  boolean setY(float inY) { 
    y = inY;
    setY = true;
    if (setX & setY & setW & setH) { 
      _set(true);
    }
    return(set);
  }

  boolean setW(float inW) { 
    w = inW;
    setW = true;
    if (setX & setY & setW & setH) { 
      _set(true);
    }
    return(set);
  }

  boolean setH(float inH) { 
    h = inH;
    setH = true;
    if (setX & setY & setW & setH) { 
      _set(true);
    }
    return(set);
  }

  float getX() { 
    return x;
  }
  float getY() { 
    return y;
  }
  float getW() { 
    return w;
  }
  float getH() { 
    return h;
  }

  Boundary expand(float inX, float inY, float inW, float inH) {
    if (inX < x) {
      x = inX;
    }
    if (inY < y) { 
      y = inY;
    }
    //if ((inW + inX) > (x + w)) { 
    w += inW;
    //}
    if ((inH + inY) > (y + h)) { 
      h = inH;
    }
    return(this);
  }

  boolean inside(float inX, float inY) {
    return(inX >= x && inX <= (x + w) && inY >= y && inY <= (y + h));
  }

  boolean isSet() { 
    return(set);
  }
}