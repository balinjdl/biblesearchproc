class BibleUtils {
  int BOOK_NAMES_ALL = 0;
  int BOOK_NAMES_NT = 1;

  int activeBookNames = BOOK_NAMES_ALL;

  String[][] book_names = {
    {
      "Gen", "Exo", "Lev", "Num", "Deu", "Jos", "Jdg", "Rut", 
      "1Sa", "2Sa", "1Ki", "2Ki", "1Ch", "2Ch", "Ezr", "Neh", 
      "Est", "Job", "Psa", "Pro", "Ecc", "Sol", "Isa", "Jer", 
      "Lam", "Eze", "Dan", "Hos", "Joe", "Amo", "Oba", "Jon", 
      "Mic", "Nah", "Hab", "Zep", "Hag", "Zec", "Mal", 
      "Mat", "Mar", "Luk", "Joh", "Act", "Rom", "1Co", "2Co", 
      "Gal", "Eph", "Phi", "Col", "1Th", "2Th", "1Ti", "2Ti", 
      "Tit", "Phm", "Heb", "Jam", "1Pe", "2Pe", "1Jo", "2Jo", 
      "3Jo", "Jud", "Rev"
    }, 
    {
      "Matt", "Mark", "Luke", "John", "Acts", "Rom", 
      "1 Cor", "2 Cor", "Gal", "Eph", "Phil", 
      "Col", "1 Thess", "2 Thess", "1 Tim", "2 Tim", 
      "Titus", "Phlm", "Heb", "Jas", "1 Pet", "2 Pet", 
    "1 John", "2 John", "3 John", "Jude", "Rev" }
  };

  BibleUtils() {
  }

  String[] getBookNames() {
    return(book_names[activeBookNames]);
  }
}  