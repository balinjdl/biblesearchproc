class Chapter {
  int number;
  Verse[] verses;
  int wordCount = -1;
  int letterCount = -1;
  Boundary boundary;
  Book book;

  Chapter() {
    verses = new Verse[0];
    boundary = new Boundary();
  }

  Chapter(Verse[] v) {
    verses = v;
    book = v[0].book;
  }

  Chapter setBook(Book inBook) {
    book = inBook;
    return(this);
  }

  Book getBook() { 
    return(book);
  }

  boolean hasBoundary() {
    return(boundary.isSet());
  }

  boolean mouseInside(float x, float y) {
    if (hasBoundary()) {
      return(boundary.inside(x, y));
    } else {
      //println("Boundary not set for Book #" + book.name + " Chp #" + str(number));
      return(false);
    }
  }

  Chapter setBoundary(Boundary b) { 
    //if (hasBoundary()) {
    //  boundary = null;
    //}
    boundary = b;
    return(this);
  }

  Chapter setNumber(int inNum) { 
    number = inNum; 
    return(this);
  }

  void display(Search search) {
    if (search.hitInChapter(this.book.number, this.number)) {
      stroke(view.COLOR_BORDER_DEFAULT);
      fill(view.COLOR_HIT_IN_CHAPTER);
    } else {
      if (view.showHitsOnly) {
        stroke(view.COLOR_BORDER_LIGHT);
        fill(view.COLOR_TRANSPARENT);
      } else {
        stroke(view.COLOR_BORDER_DEFAULT);
        fill(view.COLOR_FILL_DEFAULT);
      }
    }
    rect(boundary.x, boundary.y, boundary.w, boundary.h);
  }

  Chapter addVerse(Verse v) {
    verses = (Verse[]) expand(verses, verses.length + 1);
    verses[verses.length - 1] = v;
    return(this);
  }

  String[] getVersesAsStrings() {
    String[] result = new String[verses.length];
    for (Verse ver : verses) {
      result[ver.verse - 1] = str(ver.verse) + ": " + ver.content;
    }
    return(result);
  }

  Verse[] getVerses() {
    return(verses);
  }

  int getWordCount() {
    if (wordCount <= 0) {
      wordCount = 0;
      for (Verse v : verses) {
        wordCount += v.getWordCount();
      }
    }
    return(wordCount);
  }

  String toString() {
    String result = "";
    for (Verse ver : verses) {
      result += str(ver.verse) + ": " + ver.content + " ";
    }
    return(result);
  }

  String toString(Search search, boolean showHitsOnly) {
    String result = "";
    for (Verse ver : verses) {
      result += search.highlight(ver, showHitsOnly);
    }
    return(result);
  }
}