class Search {
  String str = "";
  boolean searching = false;

  boolean inParens = false;

  SearchStats stats;

  Search() {
    stats = new SearchStats();
    searching = false;
  }

  Search(String searchString) {
    stats = new SearchStats();
    setSearchTerm(searchString);
    searching = true;
  }

  Search setSearchTerm(String searchString) {
    str = searchString;
    searching = true;
    return(this);
  }

  Search start() {
    searching = true;
    str = "";
    return(resetStats());
  }

  Search start(String inStr) {
    searching = true;
    str = inStr;
    return(this);
  }

  Search restart() {
    searching = true;
    return(resetStats());
  }

  Search stop() {
    searching = false;
    return(freezeStats());
  }

  Search shorten() {
    if (str.length() > 0) {
      println("str = " + str + "; length = " + str.length());
      if (inParens || str.substring(str.length()) == ")") {
        this.inParens = true;
        str = str.substring(0, str.length() - 1);
        return(this);
      } else if (str.substring(str.length()) == "(") {
        this.inParens = false;
      }
      str = str.substring(0, str.length() - 1);
    }
    return(restart());
  }

  Search append(String s) {
    str = str + s;
    return(restart());
  }

  Search append(char c) {
    str = str + str(c);
    return(resetStats());
  }

  String toString() { 
    return(str);
  }
  boolean searching() {
    return(searching);
  }

  Search resetStats() {
    stats.reset();
    return(this);
  }

  Search freezeStats() {
    stats.freeze();
    return(this);
  }

  boolean hitInVerse(Verse v) {
    return(stats.hits_v.contains(v));
  }

  boolean hitInChapter(Chapter chp) {
    return(hitInChapter(chp.book.number, chp.number));
  }
  
  boolean hitInChapter(int bookNum, int chpNum) {
    for (SearchHit h : stats.hits) {
      if (chpNum == h.v.chapter && bookNum == h.v.book.number) {
        //println("Found match in hitInChapter(" + str(bookNum) + " " + str(chpNum) + ")");
        return(true);
      }
    }
    return(false);
  }

  String highlight(Verse ver, boolean showHitsOnly) {
    //println("hitInChapter(" + str(ver.book.number) + ", " + str(ver.chapter) + "? --> " + hitInChapter(ver.book.number, ver.chapter) + " <-- " + stats.getHitsFlat() + " --> Count = " + str(stats.count()));
    //if (hitInChapter(ver.book.number, ver.chapter)) {
    if (hitInVerse(ver) && ver.matches.length > 0) {
      if (str.indexOf("(") > 0) {
        String[] results = ver.content.split(ver.matches[1]);
        return(str(ver.verse) + ": " + join(results, "***" + ver.matches[1] + "***") + (showHitsOnly ? "\n" : ""));
      } else {
        String[] results = ver.content.split(str);
        return(str(ver.verse) + ": " + join(results, "***" + str + "***") + (showHitsOnly ? "\n" : ""));
      }
    } else {
      if (!showHitsOnly) {
        return(" " + str(ver.verse) + ": " + ver.content);
      } else {
        return("");
      }
    }
  }

  boolean anyHits() { 
    return(stats.hits.size() > 0);
  }
}